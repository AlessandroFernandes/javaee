package conexao;



import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

public class ContatoDao {
	
	private java.sql.Connection connection;
	
	public ContatoDao(){
		this.connection = new ConnectionFactory().getConnection();
	}
	
	public void adiciona(Contatos contato){
		String sql = "insert into contatos" +
					"(nome, email, endereco, dataNascimento)" +
				    "values (?,?,?,?)";
		
		try{
			java.sql.PreparedStatement stmt =  connection.prepareStatement(sql);
			stmt.setString(1, contato.getNome());
			stmt.setString(2, contato.getEmail());
			stmt.setString(3, contato.getEndereco());
			stmt.setDate(4, new java.sql.Date(
					Calendar.getInstance().getTimeInMillis()));
			stmt.execute();
			stmt.close();
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	public void alterar(Contatos contato){
		String sql = "update contatos set nome=?, email=?, endereco=?, dataNascimento=? where id=?";
		
		try{
			java.sql.PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, contato.getNome());
			stmt.setString(2, contato.getEmail());
			stmt.setString(3, contato.getEndereco());
			stmt.setDate(4, new java.sql.Date(
					Calendar.getInstance().getTimeInMillis()));
			stmt.setLong(5, contato.getId());
			stmt.execute();
			stmt.close();
		}catch(Exception e){
			throw new RuntimeException(e);
			
		}
		
	}
	
	public void remover (Contatos contato){
		try{
			java.sql.PreparedStatement stmt = connection.prepareStatement("delete from contatos where id=?");
			stmt.setLong(1, contato.getId());
			stmt.execute();
			stmt.close();
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	public void pesquisa(){
		try{
			java.sql.PreparedStatement stmt = connection.prepareStatement("select * from contatos");
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()){
				Long id = rs.getLong("id");
				String nome = rs.getString("nome");
				String email = rs.getString("email");
				String endereco = rs.getString("endereco");
				
				System.out.println(id + " :: " + nome + " :: " + email + " :: " + endereco );
			}
			
			rs.close();
			stmt.close();
			
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	public List<Contatos> getLista(){
		try{
			List<Contatos> contatos = new ArrayList<Contatos>();
			PreparedStatement stmt = (PreparedStatement) this.connection.prepareStatement("select * from contatos");
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()){
				Contatos contato = new Contatos();
				
				contato.setId(rs.getLong("id"));
				contato.setNome(rs.getString("nome"));
				contato.setEmail(rs.getString("email"));
				contato.setEndereco(rs.getString("endereco"));
				
				Calendar data = Calendar.getInstance();
				data.setTime(rs.getDate("dataNascimento"));
				contato.setDataNascimento(data);
				
				contatos.add(contato);
			}
			rs.close();
			stmt.close();
			return contatos;
		}catch(SQLException e){
			throw new RuntimeException(e);
				}
		
		
	}
}
