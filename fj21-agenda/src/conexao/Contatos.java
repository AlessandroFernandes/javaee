package conexao;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Contatos {

	private long id;
	private String nome;
	private String email;
	private String endereco;
	private Calendar dataNascimento;
	
	public long getId (){
		return this.id;
	}
	public void setId (long novo){
		this.id = novo;
	}
	
	public String getNome(){
		return this.nome;
	}
	public void setNome(String novo){
		this.nome = novo;
	}
	
	public String getEmail(){
		return this.email;
	}
	public void setEmail(String novo){
		this.email = novo;
	}
	
	public String getEndereco(){
		return this.endereco;
	}
	public void setEndereco(String novo){
		this.endereco = novo;
	}
	
	public Calendar getDataNascimento(){
		return this.dataNascimento;
	}
	public void setDataNascimento(Calendar nova){
		this.dataNascimento = nova;
	}
	
}
