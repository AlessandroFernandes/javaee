package conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class ConexaoTeste {

	public Connection getConnetion(){
		
		try{
			return DriverManager.getConnection(
					"jdbc:mysql://localhost/fj21", "root", "sandro");
		}catch(SQLException e){
			throw new RuntimeException(e);
		}
		
	}
}
