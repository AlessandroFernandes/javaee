package conexao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAOFuncionario {
	
	private java.sql.Connection connection;
	
	public DAOFuncionario(){
		this.connection = new ConexaoTeste().getConnetion();
	}
	
	public void adicionar(Funcionario funcionario) throws SQLException{
		
		String sql = "insert into funcionario" + "(nome, login, senha)" + "values (?,?,?)"; 
		
		try(PreparedStatement stmt = connection.prepareStatement(sql)){
			stmt.setString(1, funcionario.getNome());
			stmt.setString(2, funcionario.getLogin());
			stmt.setString(3, funcionario.getSenha());
			
			stmt.execute();
			
		}catch(Exception e){
			throw new RuntimeException(e);
		}
		
	}

	public List<Funcionario> getList() throws SQLException{
		
		try{
		List<Funcionario> funcionarios = new ArrayList();
		PreparedStatement stmt = connection.prepareStatement("select * from funcionario");
		ResultSet rs = stmt.executeQuery();
		
		while(rs.next()){
			Funcionario funcionario = new Funcionario();
			
			funcionario.setId(rs.getLong("id"));
			funcionario.setNome(rs.getString("nome"));
			funcionario.setLogin(rs.getString("login"));
			funcionario.setSenha(rs.getString("senha"));
			
			funcionarios.add(funcionario);
		}
		rs.close();
		stmt.close();
		return funcionarios;
	}catch(SQLException e){
		throw new RuntimeException(e);
	}
	}
}
