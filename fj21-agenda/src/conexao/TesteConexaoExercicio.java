package conexao;

import java.sql.SQLException;
import java.util.List;

public class TesteConexaoExercicio {

	/**
	 * @param args
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		DAOFuncionario df = new DAOFuncionario();
		
		List<Funcionario> ff = df.getList();
		
		for(Funcionario fx : ff){
			System.out.println("id: " + fx.getId());
			System.out.println("nome: " + fx.getNome());
			System.out.println("login: " + fx.getLogin());
			System.out.println("senha: " + fx.getSenha());
		}
		System.exit(0);
	}

}
