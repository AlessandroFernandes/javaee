package conexao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

import com.mysql.jdbc.PreparedStatement;

public class TesteConexao {


	
	public static void main(String[] args) throws SQLException{
		
		/*
		Contatos contato = new Contatos();
		contato.setNome("Jessica");
		contato.setEmail("Jessica@fernandesti.com");
		contato.setEndereco("Marques de Sao Vicente 166");
		contato.setDataNascimento(Calendar.getInstance());
		*/
		
		ContatoDao misterio = new ContatoDao();
		List<Contatos> contatos = misterio.getLista();
		
		for(Contatos ct : contatos){
			System.out.println("Nome: " + ct.getNome());
			System.out.println("Email: " + ct.getEmail());
			System.out.println("Endereço: " + ct.getEndereco());
			System.out.println("Data de Nascimento: " + ct.getDataNascimento().getTime() + "\n");
		}
		
		//System.out.println("Gravado!");
		System.exit(0);
		
	}
}
